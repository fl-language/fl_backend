module.exports = function (sequelize, DataTypes) {
  return sequelize.define('word', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    engWord: {
      type: DataTypes.STRING(100),
      allowNull: false,
    },
    monWord: {
      type: DataTypes.STRING(100),
      allowNull: false,
    },
    description: {
      type: DataTypes.STRING(500),
      allowNull: true
    },
    photo: {
      type: DataTypes.STRING(250),
      allowNull: true
    },
  }, {
    sequelize,
    tableName: 'word',
    timestamps: true,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },

    ]
  });
};
