module.exports = function (sequelize, DataTypes) {
  return sequelize.define('user_word', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    userId: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'user',
        key: 'id'
      }
    },
    wordId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'word',
        key: 'id'
      }
    }
  }, {
    sequelize,
    tableName: 'user_word',
    timestamps: true,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "FK_user_idx",
        using: "BTREE",
        fields: [
          { name: "userId" },
        ]
      },
      {
        name: "FK_word_idx",
        using: "BTREE",
        fields: [
          { name: "wordId" },
        ]
      },
    ]
  });
};
