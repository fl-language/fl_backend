module.exports = function (sequelize, DataTypes) {
  return sequelize.define('user_exam', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    userWordId: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'user_word',
        key: 'id'
      }
    },
    examId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'exam',
        key: 'id'
      }
    }
  }, {
    sequelize,
    tableName: 'user_exam',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "FK_exam_idx",
        using: "BTREE",
        fields: [
          { name: "examId" },
        ]
      },
      {
        name: "FK_user_word_idx",
        using: "BTREE",
        fields: [
          { name: "userWordId" },
        ]
      },
    ]
  });
};
