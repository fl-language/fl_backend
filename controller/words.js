const MyError = require("../utils/myError")
const asyncHandler = require('express-async-handler')
const paginate = require("../utils/paginate-sequelize");

exports.createWord = asyncHandler(async (req, res, next) => {
    console.log(req.body);
    const word = await req.db.word.create(req.body);
    res.status(200).json({
        success: true,
        data: word
    })
})

exports.getWords = asyncHandler(async (req, res, next) => {
    const page = parseInt(req.query.page) || 1;
    const limit = parseInt(req.query.limit) || 50;
    let select = req.query.select;
    const sort = req.query.sort;
    if (select) {
        select = select.split(' ')
    }
    ['select', 'sort', 'page', 'limit'].forEach(el => delete req.query[el])
    const pagination = await paginate(page, limit, req.db.word);
    let query = { offset: pagination.start - 1, limit };

    if (req.query) {
        query.where = req.query;
    }
    if (select) {
        query.attributes = select
    }
    if (sort) {
        query.order = sort.split(" ").map(el => [el.charAt(0) === '-' ? el.substring(1) : el, el.charAt(0) === "-" ? "DESC" : "ASC"])
    }
    let words = await req.db.word.findAll(query);
    res.status(200).json({
        success: true,
        data: words,
        pagination
    })
})


exports.getWord = asyncHandler(async (req, res, next) => {

    let word = await req.db.word.findByPk(req.params.id);
    if (!word)
        throw new MyError(`${req.params.id} id тэй үг олдсонгүй`, 400)
    const user = await word.getUser();
    res.status(200).json({
        success: true,
        user,
        data: word
    })
})

exports.updateWord = asyncHandler(async (req, res, next) => {

    let word = await req.db.word.findByPk(req.params.id);
    if (!word)
        throw new MyError(`${req.params.id} id тэй үг олдсонгүй`, 400)
    word = await word.update(req.body)
    res.status(200).json({
        success: true,
        data: word
    })
})

exports.deleteWord = asyncHandler(async (req, res, next) => {

    let word = await req.db.word.findByPk(req.params.id);
    if (!word)
        throw new MyError(`${req.params.id} id тэй үг олдсонгүй`, 400)
    word = await word.destroy();
    res.status(200).json({
        success: true,
        data: word
    })
})