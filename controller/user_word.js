const MyError = require("../utils/myError")
const asyncHandler = require('express-async-handler')
const paginate = require("../utils/paginate-sequelize");

exports.createUserWord = asyncHandler(async (req, res, next) => {
    console.log(req.body);
    const userWord = await req.db.user_word.create(req.body);
    res.status(200).json({
        success: true,
        data: userWord
    })
})

exports.getUserWords = asyncHandler(async (req, res, next) => {
    const page = parseInt(req.query.page) || 1;
    const limit = parseInt(req.query.limit) || 4;
    let select = req.query.select;
    const sort = req.query.sort;
    if (select) {
        select = select.split(' ')
    }
    ['select', 'sort', 'page', 'limit'].forEach(el => delete req.query[el])
    const pagination = await paginate(page, limit, req.db.user_word, req.query);
    let query = { offset: pagination.start - 1, limit };

    if (req.query) {
        query.where = req.query;
    }
    if (select) {
        query.attributes = select
    }
    if (sort) {
        query.order = sort.split(" ").map(el => [el.charAt(0) === '-' ? el.substring(1) : el, el.charAt(0) === "-" ? "DESC" : "ASC"])
    }
    let userWords = await req.db.user_word.findAll({ ...query, include: [req.db.user, req.db.word] });
    res.status(200).json({
        success: true,
        data: userWords,
        pagination
    })
})

exports.getUserWord = asyncHandler(async (req, res, next) => {
    let userWord = await req.db.user_word.findByPk(req.params.id);
    if (!userWord)
        throw new MyError(`${req.params.id} id тэй хэрэгэлэгчийн үг олдсонгүй`, 400)
    const user = await userWord.getUser();
    const word = await userWord.getWord();
    res.status(200).json({
        success: true,
        user,
        word,
        data: userWord
    })
})
exports.deleteUserWord = asyncHandler(async (req, res, next) => {
    console.log(req.body);
    let userWord = await req.db.user_word.findByPk(req.params.id);
    if (!userWord)
        throw new MyError(`${req.params.id} id тэй коммэнт олдсонгүй`, 400);
    userWord = await userWord.destroy();
    res.status(200).json({
        success: true,
        data: userWord
    })
})
