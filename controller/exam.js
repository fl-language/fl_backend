const MyError = require("../utils/myError")
const asyncHandler = require('express-async-handler')
const paginate = require("../utils/paginate-sequelize");

exports.createExam = asyncHandler(async (req, res, next) => {
    console.log(req.body);
    const exam = await req.db.exam.create(req.body);
    res.status(200).json({
        success: true,
        data: exam
    })
})