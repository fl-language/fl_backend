const express = require("express")
// const { protect, authorize } = require("../middleware/protect")
const { createExam } = require("../controller/exam")

const router = express.Router();
router.route("/").post(createExam);

module.exports = router;