const express = require("express")
// const { protect, authorize } = require("../middleware/protect")
const { createWord, getWords, getWord, updateWord, deleteWord } = require("../controller/words")

const router = express.Router();
router.route("/").post(createWord).get(getWords);
router.route("/:id").get(getWord).put(updateWord).delete(deleteWord);

module.exports = router;