const express = require("express")
// const { protect, authorize } = require("../middleware/protect")
const { createUserWord, getUserWords, getUserWord, deleteUserWord } = require("../controller/user_word")

const router = express.Router();
router.route("/").post(createUserWord).get(getUserWords);
router.route("/:id").get(getUserWord).delete(deleteUserWord);

module.exports = router;